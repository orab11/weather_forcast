import axios from 'axios';
import React, { useEffect, useState } from 'react'

export default function CurrencyConvertor() {
    const [currencyData,setCurrencyData] = useState([]);
    const [inputCurrency, setInputCurrency] =useState(1);
    const [fromCurrency, setFromCurrency] = useState("SGD");
    const [toCurrency, setToCurrency] = useState("INR");
    const [resultAmount, setResultAmount] = useState();

    useEffect(()=>{
        const dataFetch = async()=>{
            try{
                const response =await axios.get(`https://open.er-api.com/v6/latest/${fromCurrency}`)
                console.log(response);
                setCurrencyData(response.data.rates);
                console.log(currencyData);
            }
            catch(error){
                console.error(error);
            }
        }
        dataFetch();
    },[fromCurrency, toCurrency]);

    

    const handleConvert = ()=>{
        const result = (inputCurrency*currencyData[toCurrency]);
        console.log(result);
        setResultAmount(result);
        console.log(resultAmount);
    }
  return (
    <>
    <div className='flex flex-col items-center justify-center min-h-screen'>
        <div className='container border w-full mx-3  md:flex flex-col md:w-3/12 shadow-xl rounded *:my-3 *:mx-5 *:rounded-lg *:px-5 *:py-4'>
            <h1 className='font-bold  md:text-3xl text-center'>Currency Converter</h1>
        <label> Amount:</label>
            <input type="number" value={inputCurrency} onChange={(e)=>setInputCurrency(e.target.value)} className='border border-gray-500 m-2 p-2 rounded shadow-md'/>

           <label> From Currency:</label>
            <select value={fromCurrency} onChange={(e)=> setFromCurrency(e.target.value)}>
                {Object.keys(currencyData).map((currency, index)=>(
                    <option key={index} value={currency}>{currency}</option>
                ))}
                
            </select>

            <label>To Currency:</label>
            <select value={toCurrency} onChange={(e)=> setToCurrency(e.target.value)}>
            {Object.keys(currencyData).map((currency, index)=>(
                    <option key={index} value={currency}>{currency}</option>
                ))}
            </select>

            <button onClick={handleConvert}className=' my-5 transition ease-in-out delay-150 bg-blue-500 hover:-translate-y-1 hover:scale-100 hover:bg-indigo-500 duration-300 '>Convert Currency</button>
            <h1>{Math.round(resultAmount)}</h1>
        </div>

       

    </div>
    </>
    
  )
}
